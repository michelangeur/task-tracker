import React from 'react'
import Paper from '@material-ui/core/Paper'
import List from '@material-ui/core/List'
import Divider from '@material-ui/core/Divider'

import Task from './Task'


export default function TaskList( { data, removeTask, toggleTask, editTask } ) {
  if( data.length ){
  return (
    <Paper>
      <List>
      { data.map( (task, i) => (
        
        <>
          <Task 
            key={ task.id }
            id={ task.id }
            task={ task.text }  
            isDone={ task.isDone } 
            removeTask={ removeTask }
            toggleTask={ toggleTask }
            editTask={ editTask }
          />
          {/* hide last divider */}
        { i < data.length - 1 && <Divider /> }
        
        </>

      )) }
      </List>
      
      </Paper> 
    ) }
  return null
}
