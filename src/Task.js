import React from 'react'
import useToggleState from './hooks/useToggleState'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Checkbox from '@material-ui/core/Checkbox'
import DeleteIcon from '@material-ui/icons/Delete'
import IconButton from '@material-ui/core/IconButton'
import EditIcon from '@material-ui/icons/Edit'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'

import EditTaskForm from './EditTaskForm'


export default function Task({ id, task, isDone, removeTask, toggleTask, editTask }) {
  const [ isEditing, toggle ] = useToggleState(false)
  return (
    
    <ListItem style={{ height: "64px"}}>
      {
        isEditing ? 
          <EditTaskForm 
            editTask={editTask} 
            task={task} 
            id={id} 
            toggleEditForm={ toggle }
          /> :
          <>
          <Checkbox 
            tabIndex={-1} 
            checked={isDone} 
            onClick={ () => toggleTask(id)} />
          <ListItemText style={{  textDecoration : isDone ? 'line-through' : 'none'}}> 
            { task } 
          </ListItemText>

          <ListItemSecondaryAction>
            <IconButton aria-label='Delete' onClick={ () => removeTask(id)}>
              <DeleteIcon />
            </IconButton>

            <IconButton aria-label='Edit' onClick={toggle}>
              <EditIcon />
            </IconButton>
          </ListItemSecondaryAction>
          </>
      }
      

    </ListItem>
    
  )
}
