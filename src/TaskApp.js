import { useState, useEffect } from 'react'
import React from 'react'
import Paper from '@material-ui/core/Paper'
import AppBar from '@material-ui/core/AppBar'
import { Toolbar, Typography } from '@material-ui/core'
import Grid from '@material-ui/core/Grid'

import TaskList from './TaskList'
import TaskForm from './TaskForm'


export default function TaskApp() {

  const initialVal = JSON.parse( window.sessionStorage.getItem('tasks') || "[]")

  // const initialVal = [

  //   { id: 1, text : 'study React', isDone : false },
  //   { id: 2, text : 'Wash car', isDone : true },
  //   { id: 3, text : 'Cook Food', isDone : false }

  // ]
  const [ tasks, setTasks ] = useState(initialVal)

  useEffect( () => {
    window.sessionStorage.setItem("tasks", JSON.stringify( tasks ))
  }, [tasks])

  const addTask = newTaskText => {
    setTasks( [...tasks, { id: new Date().getTime(), text : newTaskText, isDone : false }])
  }

  const removeTask = taskId => {
    const updatedTasks = tasks.filter( task => task.id !== taskId)

    setTasks( updatedTasks )
  }

  const checkTask = taskId => {
    const updatedTasks = tasks.map( task => 
      task.id === taskId ? { ...task, isDone: !task.isDone } : task
    )
    setTasks( updatedTasks )
  }

  const editTask = ( taskId, newTask ) => {
    const updatedTasks = tasks.map( task => 
     task.id === taskId ? { ...task, text : newTask } : task 
    )
    setTasks( updatedTasks )
  }

  return (
    <Paper
      style={{
        padding: 0,
        margin: 0,
        height: "100vh",
        backgroundColor: "#fafafa"
      }}
      elevation={0}
    >
      <AppBar color='primary' position='static' style={{ height: "64px"}}>
        <Toolbar>
          <Typography color='inherit'>Tasks Go Here</Typography>
        </Toolbar>
      </AppBar>

      <Grid container justify="center" style={{ marginTop: '64px'}}>
        <Grid item xs={11} md={8} l={4}>
          <TaskForm addTask={ addTask }  />
          <TaskList 
            data={ tasks } 
            removeTask={ removeTask } 
            toggleTask={checkTask} 
            editTask={editTask}
          />
        </Grid>
      </Grid>

    </Paper>
  )
}
