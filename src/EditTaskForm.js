import React from 'react'
import useInputState from './hooks/useInputState'
import TextField from '@material-ui/core/TextField'

export default function EditTaskForm( { id, editTask, task, toggleEditForm }) {
  const [ value, handleChange, reset ] = useInputState(task)
  return (
    <form 
      onSubmit={ e => {
        e.preventDefault();
        editTask( id, value)
        reset()
        toggleEditForm()
      }}
    >
      <TextField 
        margin="normal"
        value={value}
        onChange={ handleChange }
        fullWidth
        autoFocus
      />
    </form>
    
  )
}
